import java.util.Scanner;

public class Main {

    Scanner in = new Scanner(System.in);

    public static String[] model = new String[10];

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("TODO LIST APP");

        String input = in.nextLine();
        switch (input){
            case 1:
                addToDoList();
        }
    }

    /*
    mneampilkan aplikasi todo
     */
    public static void showToDoList(){
        for(var i=0; i<model.length;i++){
            var todo = model[i];
            var no = i + 1;

            if(todo != null){
                System.out.println(no + ". " + todo);
            }
        }
    }

    public static void testShowToDoList(){
        model[0] = "belajar java dasar";
        model[1] = "membuat aplikasi todo list";
        showToDoList();
    }

    public static void addToDoList(String todo){
        var isFull = true;
        for (int i=0;i<model.length;i++){
            if(model[i]==null){
                isFull = false;
                break;
            }
        }

        if(isFull){
            var temp = model;
            model = new String[model.length *2];

            for (int i = 0; i< temp.length;i++){
                model[i] = temp[i];
            }
        }

        //tambahkan ke posisi data array yang null
        for(var i = 0;i< model.length;i++){
            if(model[i]==null){
                model[i] = todo;
                break;
            }
        }
    }

    public static void testAdd(){
        for (int i=1;i<=25;i++){
            addToDoList("contoh todo ke " + i);
        }
        showToDoList();
    }

    public static boolean removeToDoList(Integer number){
        if(number -1 >= model.length) {
            return false;
        }else if(model[number -1] == null){
            return false;
        } else {
            model[number -1 ] = null;
            return true;
        }
    }
    public static void viewShowToDoList(){

    }
    public static void viewAddToDoList(){

    }
    public static void viewRemoveToDoList(){

    }

}